<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Page d'inscription">
    <meta name="author" content="Maton Anthony">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <title> Events manager || Registration</title>
    <!-- Bootstrap core CSS -->
    <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../bootstrap/css/templates/register.css" rel="stylesheet">
  </head>

  <body>
    <div class="container">
      <form class="form-signin" role="form" method="post" action="../controller/registerAction.php">
        <h2 class="form-signin-heading">Register please !</h2>
        <input type="text" class="form-control" placeholder="Username" name="username" required autofocus>
        <input type="email" class="form-control" placeholder="Email address" name="mail" required>
        <input type="password" class="form-control" placeholder="Password" name="password"required>
        <br>
        <input type="text" class="form-control" placeholder="First Name" name="firstName" required>
        <input type="text" class="form-control" placeholder="Last Name" name="lastName" required>
        <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
      </form>
    </div> <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
      <script src="../bootstrap/js/jquery.js"></script>
      <script src="../bootstrap/js/bootstrap.js"></script>
  </body>
</html>
