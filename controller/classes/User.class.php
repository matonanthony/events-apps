<?php
error_report(-1);
class User{
    include ("config.php");
    private $userId;
    private $userName;
    private $userPassword;
    private $userMail;
    private $userCreationDate;
    private $userIsValid;
    private $userFirstName;
    private $userLastName;

    public function __construct($userName, $userPassword, $userMail, $userFirstName, $userLastName){
        $this->userName = $userName;
        // Ajouter une fonction d'encodage
        $this->userPassword = sha1($userPassword);
        $this->userMail = $userMail;
        $this->userFirstName = $userFirstName;
        $this->userLastName = $userLastName;
        $this->userIsValid = 0;
    }
    public function create(){
        try{
                $userCreationDate = date("d-m-Y");
                $userIsValid = 0;
                $pdo = new PDO("mysql:host=$host;dbname=$dbname", $user, $password);
                $req = $pdo->prepare('INSERT INTO user(userName, userPassword, userMail, userCreationDate, userIsValid, userFirstName, userLastName) VALUES(:userName, :userPassword, :userMail, :userCreationDate, :userIsValid, :userFirstName, :userLastName)');
                $req->execute(array(
                                   'userName' => $userName,
                                   'userPassword' => $userPassword,
                                   'userMail' => $userMail,
                                   'userCreationDate' => $userCreationDate,
                                   'userIsValid' => $userIsValid,
                                   'userFirstName' => $userFirstName,
                                   'userLastName' => $userLastName
                               ));
        }catch(Exception $error){
             echo 'caught exception :', $error->getMessage(), "\n"
        }
}
?>
